
# Basic GIT Tutorial

Git is a version control system used for source code management as it helps in tracking source code changes and enables multiple developers to work together on the same project.

### 1. ***Git init/status/add/commit***

   - ***git init***

     The git init command initializes a new git repository.

     ``$ git init``

   - ***git status***

     The git status command lists which files are staged, unstaged and untracked.

     ``$ git status``

   - ***git add***

     The git add command promotes changes in the working directory to the staging area.

     ``$ git add [file]``

     Stages all changes in [file] for the next commit.

     ``$ git add [directory]``

     Stages all changes in [directory]for the next commit.

   - ***git commit***

     The git commit is used to create a snapshot of the staged changes along a timeline of Git projects history.

     ``$ git commit -m 'commit message'``

### 2. ***Staging Area***

Staging area is one of the three virtual zones in a local repository namely:

- Working directory
- Staging area
- Commit area

Once changes are made to the files in Working directory, they can be promoted  to the staging area with ***git add*** command. Further using the  ***git commit*** command, these changes are commited to the main repository.  This commit is added to the Commit area.

### 3. ***Branching***

A Branch represents an independent line of development which can be used for adding new features to an ongoing project.

``$ git branch``

- Lists all of the branches in your repository.

``$ git branch [branch_name]``

- Creates a new branch called [branch_name].

``$ git branch -d [branch_name]``

- Deletes the branch safely.

``$ git branch -D [branch_name]``

- Forcefully deletes the specified branch.

``$ git branch -m [new_branch_name]``

- Renames the current branch to [new_branch_name]

``$ git branch -a``

- Lists all remote branches.

### 4. ***Merging***

***git merge*** command merges independent lines of development created my ***git branch*** into a single branch. It combines sequences of commits into one unified history of commits. 

``$ git merge new-feature``

- Here, new-feature is a branch that is getting merged with the current branch.

### 5. ***Install Tig***

Tig is a text-mode interface for git and serves as a Git repository browser.

``$ sudo apt install tig``

- Installs tig on ubuntu.

``$ tig``

- Opens an interactive window displaying each commit.

### 6. ***Create a remote repository on Gitlab***

- Register on Gitlab using the email provided mountblue.

- In your terminal, add your username:

``$ git config --global user.name "your_username"``
- Add your email address:

``$ git config --global user.email "your_email_address@example.com"``

- In Gitlab create a project to hold your files.

- In your terminal, cd into your git repository, replace ***username/projectpath*** with your ***username/projectpath*** and run:

``$ git remote add origin git@gitlab.com:username/projectpath.git``

- To view your remote repositories:

``$ git remote -v``

### 7. ***Push to Gitlab***

- In your terminal, type:

``$ touch a.txt``

``$ git add .``

``$ git commit -m 'added a.txt'``

``$ git push [remote] [branch_name]``

The file a.txt has been pushed to the remote branch.




